# Зробіть таблицю для підрахунку особистих витрат із такими полями: id, призначення, сума, час.
# Створіть консольний інтерфейс (CLI) на Python для додавання нових записів до бази даних.
# Створіть агрегатні функції для підрахунку загальної кількості витрат і витрат за місяць.
# Забезпечте відповідний інтерфейс користувача.
# Змініть таблицю так, щоби можна було додати не лише витрати, а й прибутки.
import re
import sqlite3 as sq
from datetime import datetime, date
from typing import Dict, List, Tuple, NamedTuple
import calendar


class Category(NamedTuple):
    """Структура категории"""
    codename: str
    name: str
    mmc_code: int
    aliases: List[str]


class Categories:
    def __init__(self, cursor):
        self._categories = self._load_categories(cursor)

    def _load_categories(self, cursor) -> List[Category]:
        """Возвращает справочник категорий расходов из БД"""
        categories = fetchall(
            "category", "codename name mmc_code aliases".split(), {}, cursor
        )
        categories = self._fill_aliases(categories)
        return categories

    def _fill_aliases(self, categories: List[Dict]) -> List[Category]:
        """Заполняет по каждой категории aliases, то есть возможные
        названия этой категории"""
        categories_result = []
        for index, category in enumerate(categories):
            aliases = category["aliases"].split(",")
            aliases = list(filter(None, map(str.strip, aliases)))
            aliases.append(category["codename"])
            aliases.append(category["name"])
            categories_result.append(Category(
                codename=category['codename'],
                name=category['name'],
                mmc_code=category['mmc_code'],
                aliases=aliases
            ))
        return categories_result

    def get_all_categories(self) -> List[Dict]:
        """Возвращает справочник категорий."""
        return self._categories

    def get_category(self, category_name: str) -> Category:
        """Возвращает категорию по одному из её алиасов."""
        finded = None
        other_category = None
        for category in self._categories:
            if category.codename == "other":
                other_category = category
            for alias in category.aliases:
                if category_name in alias:
                    finded = category
        if not finded:
            finded = other_category
        return finded


def error(message):
    print("Error: {} Try again.".format(message))


def get_timestamp(y, m, d, h, s):
    return datetime.timestamp(datetime(y, m, d, h, s))


def get_data(tmstmp):
    return datetime.fromtimestamp(tmstmp).date()


def get_str(prompt):
    while True:
        s = input(prompt).strip()
        if s:
            return s
        else:
            error("Input required.")


def get_int(prompt, min=None, max=None, maybe_zero=False):
    while True:
        s = get_str(prompt).lower()
        try:
            z = int(s)
        except ValueError:
            error("Invalid integer.")
            continue
        if not maybe_zero:
            if min is not None and z < min:
                error("Integer must be at least {}.".format(min))
                continue
            if max is not None and max < z:
                error("Integer must be at most {}.".format(max))
                continue
            if z == 0:
                error("Integer must be not equal 0.")
                continue
            return z
        else:
            if max is not None and max < z:
                error("Integer must be at most {}.".format(max))
                continue
            return z


def del_data_from_period(start_date_tm, end_date_tm, con) -> None:
    query_del_data_from_period = f'DELETE FROM payments WHERE created BETWEEN {start_date_tm} AND {end_date_tm})'
    con.cursor().execute(query_del_data_from_period)
    con.commit()


def update_data(table: str, column_value: tuple, id_column_value: tuple, con) -> None:
    query_update_data_from_period = f'UPDATE {table} SET {column_value[0]}={column_value[1]} ' \
                                    f'WHERE {id_column_value[0]}={id_column_value[1]}'
    con.cursor().execute(query_update_data_from_period)
    con.commit()


def insert_to_db(table: str, column_values: dict, conn, cursor):
    columns = ', '.join(column_values.keys())
    values = [tuple(column_values.values())]
    placeholders = ", ".join("?" * len(column_values.keys()))
    cursor.executemany(
        f"INSERT INTO {table} "
        f"({columns}) "
        f"VALUES ({placeholders})",
        values)
    conn.commit()


def fetchall(table: str, columns: List[str], add_if_dict: dict, cursor) -> List[Tuple]:
    columns_joined = ', '.join(columns)
    main_query_str = f'SELECT {columns_joined} FROM {table}'
    for i in range(len(add_if_dict)):
        fields_lst = list(add_if_dict.keys())
        field = fields_lst[i]
        add_if_str = ''
        if len(add_if_dict[field]) > 1:
            for y in range(len(add_if_dict[field])):
                add_if_str += f'{field} {add_if_dict[field][y][0]} {str(add_if_dict[field][y][1])}'
                if y != len(add_if_dict[field]) - 1:
                    add_if_str += f' AND '
        else:
            add_if_str += f'{field} {add_if_dict[field][0][0]} {str(add_if_dict[field][0][1])}'
            if i != len(add_if_dict) - 1:
                add_if_str += f' AND '
        main_query_str += f' WHERE {add_if_str}'
    cursor.execute(main_query_str)
    rows = cursor.fetchall()
    result = []
    for row in rows:
        dict_row = {}
        for index, column in enumerate(columns):
            dict_row[column] = row[index]
        result.append(dict_row)
    return result


def init_db():
    with sq.connect('finance.db') as con:
        cur = con.cursor()
        with open("createdb.sql", "r") as f:
            sql = f.read()
        cur.executescript(sql)

        start_cat_lst = [("salary", "зарплата", 3568, "зп, аванс, премия, бонус, зарплата"),
                         ("products", "продукты", 1000, "еда, химия, питание, одежда, хозтовары, хозпродукты"),
                         ("rest", "отдых", 5099, "боулинг, теннис, загородний клуб, сауна, кино, зоопарк"),
                         ("car", "авто", 1090, "техосмотр, страховка, бензин, расходники, сто"),
                         ("fitness", "спорт", 2000, "абонемент, зал, бассейн, тренер"),
                         ("doctor", "здоровье", 2134, "осмотр у врача, анализы, лекарства, клиника"),
                         ("dinner", "обед", 5814, "столовая, ланч, бизнес-ланч, бизнес ланч, завтрак"),
                         ("cafe", "кафе", 5812,
                          "ресторан, рест, мак, макдональдс, макдак, kfc, ilpatio, il patio, кафешка"),
                         ("transport", "общ. транспорт", 4111, "метро, автобус, metro, проездной, проезд"),
                         ("taxi", "такси", 4121, "bolt такси, uber taxi"),
                         ("phone", "телефон", 4814, "теле2, связь, мобильный, счет мобильного"),
                         ("books", "книги", 5192, "литература, литра, лит-ра"),
                         ("internet", "интернет", 4816, "инет, inet, провайдер"),
                         ("subscriptions", "подписки", 4899, "подписка, ютуб-премиум, мегого"),
                         ("communal", "коммуналка", 4468,
                          "коммуналка, за газ, за воду, жкх, вывоз мусора, отопление,электричество, yasno"),
                         ("charity", "благотворительность", 2789, "на зсу, помощь, на карту, на приют, на церковь"),
                         ("other", "прочее", 4789, "")]
        query = "INSERT INTO category VALUES(?,?,?,?);"
        cur.executemany(query, start_cat_lst)

        category = Categories(cursor=cur).get_category('salary')
        insert_to_db('payments', {'amount': 2500,
                                  'created': get_timestamp(2022, 4, 16, 14, 30),
                                  'raw_text': 'зп (аванс)',
                                  'category_codename': category.codename}, con, cur)
        start_data_to_table = [('7500', get_timestamp(2022, 4, 30, 16, 30), 'зп', category.codename),
                               ('-250', get_timestamp(2022, 4, 30, 20, 30), 'инет',
                                Categories(cursor=cur).get_category('internet').codename),
                               ('-250', get_timestamp(2022, 5, 31, 20, 30), 'инет',
                                Categories(cursor=cur).get_category('internet').codename),
                               ('-250', get_timestamp(2022, 6, 30, 20, 30), 'инет',
                                Categories(cursor=cur).get_category('internet').codename),
                               ('-100', get_timestamp(2022, 7, 26, 22, 30), 'ютуб-премиум',
                                Categories(cursor=cur).get_category('subscriptions').codename),
                               ('-250', get_timestamp(2022, 7, 31, 20, 30), 'инет',
                                Categories(cursor=cur).get_category('internet').codename),
                               ('12500', get_timestamp(2022, 8, 15, 17, 00), 'зп (аванс)', category.codename),
                               ('-2500', get_timestamp(2022, 8, 16, 3, 30), 'на зсу',
                                Categories(cursor=cur).get_category('charity').codename),
                               ('-100', get_timestamp(2022, 8, 26, 22, 30), 'ютуб-премиум',
                                Categories(cursor=cur).get_category('subscriptions').codename),
                               ('12500', get_timestamp(2022, 8, 31, 16, 30), 'зп', category.codename),
                               ('-250', get_timestamp(2022, 8, 31, 20, 30), 'инет',
                                Categories(cursor=cur).get_category('internet').codename),
                               ('-2000', get_timestamp(2022, 9, 2, 18, 30), 'продукты',
                                Categories(cursor=cur).get_category('продукты').codename),
                               ('-1000', get_timestamp(2022, 9, 5, 8, 30), 'на зсу',
                                Categories(cursor=cur).get_category('charity').codename),
                               ('-2000', get_timestamp(2022, 9, 9, 18, 30), 'продукты',
                                Categories(cursor=cur).get_category('продукты').codename),
                               ('-1000', get_timestamp(2022, 9, 12, 8, 30), 'на зсу',
                                Categories(cursor=cur).get_category('charity').codename),
                               ('12500', get_timestamp(2022, 9, 15, 17, 00), 'зп (аванс)', category.codename),
                               ('-3000', get_timestamp(2022, 9, 16, 18, 30), 'химия',
                                Categories(cursor=cur).get_category('химия').codename),
                               ('-1000', get_timestamp(2022, 9, 19, 8, 30), 'на зсу',
                                Categories(cursor=cur).get_category('charity').codename),
                               ('-4500', get_timestamp(2022, 9, 19, 9, 30), 'жкх',
                                Categories(cursor=cur).get_category('жкх').codename),
                               ('-2500', get_timestamp(2022, 9, 23, 18, 30), 'продукты',
                                Categories(cursor=cur).get_category('продукты').codename),
                               ('-1000', get_timestamp(2022, 9, 26, 8, 30), 'на зсу',
                                Categories(cursor=cur).get_category('charity').codename),
                               ('-100', get_timestamp(2022, 9, 26, 22, 30), 'ютуб-премиум',
                                Categories(cursor=cur).get_category('subscriptions').codename),
                               ('12500', get_timestamp(2022, 9, 30, 16, 30), 'зп', category.codename),
                               ('-2000', get_timestamp(2022, 9, 30, 18, 30), 'продукты',
                                Categories(cursor=cur).get_category('продукты').codename),
                               ('-250', get_timestamp(2022, 9, 30, 20, 30), 'инет',
                                Categories(cursor=cur).get_category('internet').codename),
                               ('-1000', get_timestamp(2022, 10, 3, 8, 30), 'на зсу',
                                Categories(cursor=cur).get_category('charity').codename)]
        query_2 = "INSERT INTO payments VALUES(NULL,?,?,?,?);"
        cur.executemany(query_2, start_data_to_table)
        con.commit()


def check_db_exists():
    """Проверяет, инициализирована ли БД, если нет — инициализирует"""
    conn = sq.connect("finance.db")
    cursor = conn.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='payments'")
    table_exists = cursor.fetchall()
    if table_exists:
        return
    init_db()


def get_period_total(start_date, end_date, cur, fl_debit=True):
    if fl_debit:
        query_total = '''SELECT sum(amount) FROM payments                                    
                                 WHERE (created >= %(from)d)
                                 AND (created <= %(to)d) AND (amount > 0)
                                 ORDER BY created
                                 ''' % {'from': start_date, 'to': end_date}
        cur.execute(query_total)
        for result in cur.fetchall():
            if result[0]:
                if result[0] > 0:
                    return result[0]
        return ''
    else:
        query_total_credit = '''SELECT sum(amount) FROM payments
                                     WHERE (created >= %(from)d)
                                     AND (created <= %(to)d) AND (amount < 0)
                                     ORDER BY created
                                         ''' % {'from': start_date, 'to': end_date}
        cur.execute(query_total_credit)
        for res in cur.fetchall():
            if res[0]:
                if res[0] < 0:
                    return -res[0]
        return ''


def get_period_report(m_num, cur, d_num=1, flg_period=1):
    month_list = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь',
                  'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь']
    _period = ['all', 'per_month', 'per_day'][flg_period]
    start_date_period_stmp = get_timestamp(date.today().year, m_num, d_num, 0, 0)
    match _period:
        case 'per_day':
            end_date_period_stmp = get_timestamp(date.today().year, m_num, d_num, 23, 59)
            selected_period = f'дату {datetime.fromtimestamp(start_date_period_stmp).strftime("%m/%d/%Y")}'
        case 'per_month':
            _, days_in_month = calendar.monthrange(date.today().year, m_num)
            end_date_period_stmp = get_timestamp(date.today().year, m_num, days_in_month, 23, 59)
            selected_period = f'{month_list[int(m_num) - 1]} месяц'
        case 'all':
            _, days_in_month = calendar.monthrange(date.today().year, date.today().month)
            end_date_period_stmp = get_timestamp(date.today().year, date.today().month, days_in_month, 23, 59)
            selected_period = f'период c {month_list[int(m_num) - 1]} по {month_list[int(date.today().month) - 1]}'

    query_data_from_period = '''SELECT amount, created, raw_text, category.name, category.mmc_code FROM payments
                             LEFT JOIN category ON category.codename = payments.category_codename 
                             WHERE (created >= %(from)d) AND (created <= %(to)d) ORDER BY created
                             ''' % {'from': start_date_period_stmp, 'to': end_date_period_stmp}
    cur.execute(query_data_from_period)
    print('\n{:^101}'.format(
        f' Баланс за {selected_period}'))
    print('{:-^101}'.format('-'))
    print('|' + '{:^14}'.format(' ДАТА ') + '|', '{:^12}'.format(' ПРИХОД ') + '|', end='')
    print('{:^12}'.format(' РАСХОД ') + '|', '{:^22}'.format(' КАТЕГОРИЯ ') + '|', end='')
    print('{:^6}'.format('  MMC ') + '|', '{:^25}'.format(' КОММЕНТАРИЙ ') + '|', end='')
    print('\n{:-^101}'.format('-'))

    for res in cur.fetchall():
        if res[0]:
            debit = credit = ''
            row_date = get_data(res[1])
            if res[0] < 0:
                credit = -res[0]
            elif res[0] > 0:
                debit = res[0]

            print('|' + '{:^14}'.format(f' {row_date} ') + '|', '{:^12}'.format(f' {debit} ') + '|', end='')
            print('{:^12}'.format(f' {credit} ') + '|', '{:^22}'.format(f' {res[3]} ') + '|', end='')
            print('{:^6}'.format(f' {res[4]} ') + '|', '{:^25}'.format(f' {res[2]} ') + '|', end='')
            print('\n{:-^101}'.format('-'))

    total_credit = get_period_total(start_date_period_stmp, end_date_period_stmp, cur, False)
    total_debit = get_period_total(start_date_period_stmp, end_date_period_stmp, cur)

    print('|' + '{:^14}'.format(' ИТОГО: ') + '|', '{:^12}'.format(f' {total_debit} ') + '|', end='')
    print('{:^12}'.format(f' {total_credit} ') + '|', '{:^22}'.format('  ') + '|', end='')
    print('{:^6}'.format('  ') + '|', '{:^25}'.format('  ') + '|', end='')
    print('\n{:-^101}'.format('-'))


def operations_with_categories(cur):
    while True:
        print('\n{:20}'.format('1. Вывести весь справочник'))
        print('{:20}'.format('2. Добавить данные (в разработке)'))
        print('{:20}'.format('3. Изменить данные (в разработке)'))
        print('\n{:>20}'.format('\t[4] Назад'))
        get_act_4 = get_str('\nВведите дальнейшее действие: ')
        match get_act_4:
            case ('4' | '2' | '3'):
                break
            case '1':
                categories_lst = Categories(cur).get_all_categories()
                print('\n{:^101}'.format(f' Справочник категорий доходов/расходов'))
                print('{:*^101}'.format('*'))
                print('{:^18}'.format(' codename ') + '|', '{:^22}'.format(' name ') + '|', end='')
                print('{:^9}'.format(' mmc_code ') + '|', '{:^55}'.format('alias(слова относящиеся к категории)'),
                      end='')
                print('\n{:-^101}'.format('-'))
                for category in categories_lst:
                    print('{:^18}'.format(f' {category.codename} ') + '|',
                          '{:^22}'.format(f' {category.name} ') + '|', end='')
                    print('{:^10}'.format(f'  {category.mmc_code}  ') + '|',
                          '{:65}'.format(f' {", ".join(category.aliases)} '), end='')
                    print('\n{:-^101}'.format('-'))


def operations_with_data(action, con):
    date_from_user = input('дату (например: "09/06/2021", [Enter] - сегодня): ').lower().strip()
    tm_now = datetime.now()
    date_month = tm_now.month
    date_day = tm_now.day
    created = get_timestamp(tm_now.year, tm_now.month, tm_now.day, tm_now.hour, tm_now.minute)
    start_date = get_timestamp(tm_now.year, tm_now.month, tm_now.day, 0, 0)
    end_date = get_timestamp(tm_now.year, tm_now.month, tm_now.day, 23, 59)
    if date_from_user != '':
        date_lst = list(map(int, re.split('[/\-\.]', date_from_user)))
        if len(date_lst) > 0:
            try:
                date(date_lst[2], date_lst[1], date_lst[0])
                date_month = date_lst[1]
                date_day = date_lst[0]
                created = get_timestamp(date_lst[2], date_lst[1], date_lst[0], 15, 0)
                start_date = get_timestamp(date_lst[2], date_lst[1], date_lst[0], 0, 0)
                end_date = get_timestamp(date_lst[2], date_lst[1], date_lst[0], 23, 59)
            except ValueError as error_msg:
                error(error_msg)
    match action:
        case 'ins':
            sum_from_user = get_int('\nСумма ("+" приход/ "-" расход): ')
            cat_from_user = get_str('\nНазначение: ').lower().strip()
            insert_to_db('payments', {'amount': sum_from_user, 'raw_text': cat_from_user,
                                      'created': created,
                                      'category_codename': Categories(cursor=con.cursor()).get_category(
                                          cat_from_user).codename},
                         con, con.cursor())
        case 'del':
            del_data_from_period(start_date, end_date, con)
        case 'update':
            get_period_report(date_month, con.cursor(), date_day, 2)
            data_period = fetchall("payments", 'rowid amount created raw_text'.split(),
                                   {'created': [('>=', get_timestamp(date.today().year, date_month, date_day, 0, 0)),
                                                (
                                                    '<=',
                                                    get_timestamp(date.today().year, date_month, date_day, 23, 59))]},
                                   con.cursor())
            if not data_period:
                print('\nЗа указанный период нет данных!')
            else:
                print(f'\nНайдено {len(data_period)} записей')
                if_for_num_lst = []
                for index, row in enumerate(data_period):
                    if_for_num_lst.append(index + 1)
                    print(f'Запись [{index + 1}]: {get_data(row["created"])} | {row["amount"]} | {row["raw_text"]}')
                while True:
                    nuw_row = get_int(f'\nУкажите номер записи для редактирования или [0] для выхода:> ',
                                      1, len(data_period), True)
                    if nuw_row in if_for_num_lst:
                        print(f'\nвыбрана запись {nuw_row}')
                        date_ = input('Дата ([Enter] - оставить прежнюю): ').lower().strip()
                        if date_ == '':
                            date_ = data_period[nuw_row - 1]['created']
                            created_ = date_
                        else:
                            date_list = list(map(int, re.split('[/\-\.]', date_)))
                            if len(date_list) > 0:
                                try:
                                    date(date_lst[2], date_lst[1], date_lst[0])
                                    created_ = get_timestamp(date_list[2], date_list[1], date_list[0], 15, 0)
                                except ValueError as error_msg:
                                    error(error_msg)
                        sum_ = input('Сумма ("+" приход/ "-" расход, [Enter] - оставить прежнюю): ')
                        if sum_ == '':
                            sum_ = data_period[nuw_row - 1]['amount']
                        else:
                            sum_ = float(sum_)
                        cat_ = input('Назначение ([Enter] - оставить прежнее): ').lower().strip()
                        if cat_ == '':
                            cat_ = data_period[nuw_row - 1]['raw_text']
                        if date_ != data_period[nuw_row - 1]['created']:
                            update_data('payments', ('created', created_),
                                        ('rowid', data_period[nuw_row - 1]['rowid']), con)
                        elif sum_ != data_period[nuw_row - 1]['amount']:
                            update_data('payments', ('amount', sum_),
                                        ('rowid', data_period[nuw_row - 1]['rowid']), con)
                        elif cat_ != data_period[nuw_row - 1]['raw_text']:
                            update_data('payments', ('raw_text', cat_),
                                        ('rowid', data_period[nuw_row - 1]['rowid']), con)
                        get_period_report(date_month, con.cursor(), date_day, 2)
                        break
                    else:
                        break


def get_month_first_row_table(table: str, cur) -> int:
    query_data_from_table = f'SELECT * FROM {table} ORDER BY ROWID ASC LIMIT 1'
    cur.execute(query_data_from_table)
    for res in cur.fetchall():
        if res[0]:
            row_date = get_data(res[1])
            return row_date.month
    return 1


def main():
    check_db_exists()
    while True:
        print('\n{:_^60}'.format(' Программа учета личных финансов (на базе SQLite) '))
        print('$' * 60)
        print('\n{:20}'.format('1. Отчет за период'), '\t{:20}'.format('2. Изменить (внести) данные'))
        print('\n{:>20}'.format('\t[Q] для выхода'))
        con = sq.connect('finance.db')
        get_act = get_str('\nВведите дальнейшее действие: ')
        match get_act:
            case ('Q' | 'q' | 'й' | 'Й'):
                con.close()
                break
            case "1":
                while True:
                    print('\n{:20}'.format('1. Отчет за месяц'),
                          '{:20}'.format('2. Отчет за весь период'))
                    print('\n{:>20}'.format('\t[3] Назад'))
                    get_act_1 = get_str('\nВведите дальнейшее действие: ')
                    match get_act_1:
                        case '3':
                            break
                        case '1':
                            month_num = get_int('\nВведите номер месяца: ', 1, 12)
                            get_period_report(month_num, con.cursor())
                        case '2':
                            month_first_row = get_month_first_row_table('payments', con.cursor())
                            get_period_report(month_first_row, con.cursor(), 1, 0)
            case "2":
                while True:
                    print('\n{:20}'.format('1. Внести данные'),
                          '\t{:20}'.format('4. Cправочник категорий доходов/расходов'))
                    print('{:20}'.format('2. Удалить данные'))
                    print('{:20}'.format('3. Изменить данные'))
                    print('\n{:>20}'.format('\t[5] Назад'))
                    get_act_2 = get_str('\nВведите дальнейшее действие: ')
                    match get_act_2:
                        case '5':
                            break
                        case '1':
                            print('\nВнести данные на ', end='')
                            operations_with_data('ins', con)
                            print('Готово!')
                        case '2':
                            print('\nУдалить данные за ', end='')
                            operations_with_data('del', con)
                            print('Готово!')
                        case '3':
                            print('\nИзменить данные за ', end='')
                            operations_with_data('update', con)
                        case '4':
                            operations_with_categories(con.cursor())


if __name__ == '__main__':
    main()
