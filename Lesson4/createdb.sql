CREATE TABLE IF NOT EXISTS category(
    codename VARCHAR(255) PRIMARY KEY,
    name VARCHAR(255),
    mmc_code INTEGER,
    aliases text
);

CREATE TABLE IF NOT EXISTS payments(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    amount REAL NOT NULL,
    created INTEGER,
    raw_text text,
    category_codename integer,
    FOREIGN KEY(category_codename) REFERENCES category(codename)
);
