# Create an Exchange Rates To USD db using API Monobank (api.monobank.ua). Do requests via request lib,
# parse results, write it into db
import threading

import requests
import sqlite3 as sq
from datetime import datetime
import threading


def error(message):
    print("Error: {} Try again.".format(message))


def get_str(prompt):
    while True:
        s = input(prompt).strip()
        if s:
            return s
        else:
            error("Input required.")


def get_data(tmstmp):
    return datetime.fromtimestamp(tmstmp).date()


def init_db():
    with sq.connect('exchange.db') as con:
        cur = con.cursor()
        with open("createExchangeRateToUSDdb.sql", "r") as f:
            sql = f.read()
        cur.executescript(sql)

        start_cat_lst = [('UAH', 'Украинская гривна', 980, '₴'),
                         ('EUR', 'Евро', 978, '€'),
                         ('GBP', 'Фунт стерлингов Великобритании', 826, '£'),
                         ('JPY', 'Японская йена', 392, '¥'),
                         ('CHF', 'Швейцарский франк', 756, ''),
                         ('CNY', 'Китайский юань', 156, '¥'),
                         ('AED', 'Дирхам ОАЭ', 784, 'د.إ'),
                         ('AZN', 'Азербайджанский манат', 944, '₼'),
                         ('TRY', 'Турецкая лира', 949, '₺'),
                         ('PLN', 'Польский злотый', 985, 'Zł')
                         ]
        query = "INSERT INTO currencies VALUES(?,?,?,?);"
        cur.executemany(query, start_cat_lst)
        con.commit()


def check_db_exists():
    """Проверяет, инициализирована ли БД, если нет — инициализирует"""
    conn = sq.connect("exchange.db")
    cursor = conn.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='usd_exchange_rate'")
    table_exists = cursor.fetchall()
    if table_exists:
        return
    init_db()


def insert_to_db(table: str, column_values: dict, conn, cursor):
    columns = ', '.join(column_values.keys())
    values = [tuple(column_values.values())]
    placeholders = ", ".join("?" * len(column_values.keys()))
    cursor.executemany(
        f"INSERT INTO {table} "
        f"({columns}) "
        f"VALUES ({placeholders})",
        values)
    conn.commit()


def get_currency_name(table: str, currency_code, cur):
    query_data_from_table = f'SELECT * FROM {table} WHERE digital_code={currency_code}'
    cur.execute(query_data_from_table)
    for res in cur.fetchall():
        if res[0]:
            row_codename = res[0]
            return row_codename
    return ''


def exec_watcher():
    timer = threading.Timer(301, get_data_currency_exchange())
    timer.start()


def get_data_currency_exchange():
    try:
        with sq.connect('exchange.db') as con:
            print('Запущена автозагрузка курса USD....автообновление 5 минут')
            url_query = requests.get('https://api.monobank.ua/bank/currency')
            temp_data = url_query.json()
            if url_query.status_code == 200:
                for el in temp_data:
                    if el['currencyCodeA'] == 840 and el['currencyCodeB'] == 980:
                        insert_to_db('usd_exchange_rate', {'currency_value': el['rateSell'],
                                                           'current_date': el['date'],
                                                           'currency_name': get_currency_name('currencies',
                                                                                              el['currencyCodeB'],
                                                                                              con.cursor())},
                                     con, con.cursor())
    except Exception as err_msg:
        error(err_msg)
    finally:
        exec_watcher()


def main():
    check_db_exists()
    exec_watcher()


if __name__ == '__main__':
    main()
