CREATE TABLE IF NOT EXISTS currencies(
    codename VARCHAR(3) PRIMARY KEY,
    name VARCHAR(255),
    digital_code INTEGER,
    symbol_code VARCHAR(1)
);

CREATE TABLE IF NOT EXISTS usd_exchange_rate(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    currency_value REAL NOT NULL,
    current_date INTEGER,
    currency_name text,
    FOREIGN KEY(currency_name) REFERENCES currencies(codename)
);