# Створіть звичайну функцію множення двох чисел. Частково застосуйте її до одного аргументу.
# Створіть каррувану функцію множення двох чисел. Частково застосуйте її до одного аргументу.

'''
Карринг — это преобразование функции от многих аргументов в набор функций, каждая из которых является функцией
от одного аргумента. Мы можем передать часть аргументов в функцию и получить обратно функцию,
ожидающую остальные аргументы.

Частичное применение (partial application)
Это процесс применения функции к части ее аргументов.
Другими словами, функция, которая принимает функцию с несколькими параметрами и возвращает функцию с меньшим
количеством параметров. Частичное применение преобразует функцию от n аргументов к (x-n),
а карринг создает n функций с 1 аргументов.

Такая возможность есть у Python в стандартной библиотеки functools, это функция partial.
'''
import random
from functools import partial


def multi_func(x, y, operation):
    dict_operations = {
        '+': [lambda x, y: x + y, 'сложения чисел'],
        '-': [lambda x, y: x - y, 'вычитания чисел'],
        '*': [lambda x, y: x * y, 'умножения чисел'],
        '/': [lambda x, y: x / y, 'деления чисел'],
    }
    opr_txt = dict_operations[operation][1]
    result = dict_operations[operation][0](x, y)
    print(f'Выполнена операция {opr_txt} {x} и {y}, результат {result}')


def curry(func):
    def func_with_x(x):
        def func_with_y(y):
            def func_with_operation(operation):
                func(x, y, operation)
            return func_with_operation
        return func_with_y
    return func_with_x


if __name__ == '__main__':
    print('\n{:_^100}\n'.format(' Звичайна функція аріфметичних дій з двома числами, '
                                'частково застосована до одного аргументу '))
    new_func = partial(multi_func, random.randint(1, 100), operation='*')
    new_func(y=10)
    new_func(y=2, operation='/')
    new_func(y=200, operation='+')
    new_func(y=random.randint(1, 100), operation='-')

    print('\n{:_^100}\n'.format(' Каррована функція аріфметичних дій з двома числами, '
                                'частково застосована до одного аргументу '))
    for i in range(1, 5):
        curry(multi_func)(random.randint(1, 100))(y=i)(random.choice(['+', '-', '*', '/']))
