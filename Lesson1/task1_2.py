# Створіть список цілих чисел. Отримайте список квадратів непарних чисел із цього списку.
import random


def main():
    lst1 = [random.randint(0, 30) for _ in range(10)]
    print(f'\n исходный список {lst1}')
    print(f' список квадратов четных чисел из исходного списка {[i**2 for i in lst1 if i % 2 == 0]}')


if __name__ == '__main__':
    main()
