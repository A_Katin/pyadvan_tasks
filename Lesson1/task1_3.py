# Створіть функцію-генератор чисел Фібоначчі. Застосуйте до неї декоратор,
# який залишатиме в послідовності лише парні числа.
from functools import wraps


def del_odd_num(func):
    @wraps(func)
    def inner_func(*args):
        return [i for i in func(*args)[::] if i % 2 == 0]

    return inner_func


def fib_dp(n: int) -> int:
    a, b = 1, 2
    for _ in range(n - 1):
        a, b = b, a + b
    return a


@del_odd_num
def main():
    return list(map(fib_dp, [*range(50)]))


if __name__ == '__main__':
    print(main())
