# Створіть співпрограму, яка отримує контент із зазначених посилань і логує хід виконання в спеціальний файл,
# використовуючи стандартну бібліотеку urllib, а потім проробіть те саме з бібліотекою aiohttp.
# Кроки, які мають бути залоговані: початок запиту до адреси X, відповідь для адреси X отримано зі статусом 200.
# Перевірте хід виконання програми на >3 ресурсах і перегляньте послідовність запису логів в обох варіантах
# і порівняйте результати. Для двох видів завдань використовуйте різні файли для логування,
# щоби порівняти отриманий результат.
import asyncio
import time
import urllib.request

import aiohttp
import functools
import logging


def get_duration(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        res = func(*args, **kwargs)
        duration = time.time() - start_time
        print(f"Duration {func.__name__ } = {duration}")
        return res

    return wrapper


def set_func_logger(f_name: str) -> logging.Logger:
    logger = logging.getLogger(f_name)
    logger.setLevel(logging.INFO)
    handler = logging.FileHandler(f_name, mode='a')
    handler.setFormatter(logging.Formatter("%(name)s %(asctime)s %(levelname)s %(message)s"))
    logger.addHandler(handler)
    return logger


async def a_download_site(session, url, logger_asyncio):
    logger_asyncio.info(f"Start request to url {url}.")
    async with session.get(url) as response:
        logger_asyncio.info(f"result code: {response.status}")
        logger_asyncio.info(f"Read {response.content_length} from {url}")


async def a_download_all_sites(sites):
    logger_ = set_func_logger("asyncio")
    logger_.info('{:_^101}'.format(' Information about working module "asyncio" '))
    async with aiohttp.ClientSession() as session:
        tasks = []
        for url in sites:
            task = asyncio.ensure_future(a_download_site(session, url, logger_))
            tasks.append(task)
        await asyncio.gather(*tasks, return_exceptions=False)


def download_site(url, count, logger_urllib):
    logger_urllib.info(f"Start request to url {url}.")
    with urllib.request.urlopen(url) as response:
        logger_urllib.info(f"result code: {response.getcode()}")
        logger_urllib.info(f"{count+1}. --> Read {len(response.read().decode('utf-8'))} from {url}")


@get_duration
def download_all_sites(sites):
    logger_ = set_func_logger("urllib")
    logger_.info('{:_^101}'.format(' Information about working module "urllib" '))
    for count, url in enumerate(sites):
        download_site(url, count, logger_)


sites = [
            "https://github.com/",
            "https://openweathermap.org/",
            "http://httpbin.org/",
            "https://mocky.io",
            "https://www.jython.org"
        ] * 55

if __name__ == "__main__":
    start_time = time.time()
    print('Start asyncio version...')
    asyncio.run(a_download_all_sites(sites))
    duration = time.time() - start_time
    print(f"Duration = {duration}")

    print('\nStart urllib version...')
    download_all_sites(sites)
