# Створіть функцію для обчислення факторіала числа. Запустіть декілька завдань, використовуючи ThreadPoolExecutor,
# і заміряйте швидкість їхнього виконання, а потім заміряйте швидкість обчислення,
# використовуючи той же набір завдань на ProcessPoolExecutor. Як приклади використовуйте останні значення,
# від мінімальних і до максимально можливих, щоб побачити приріст або втрату продуктивності.
import math
import random
import functools
import time
import threading
import concurrent.futures

thread_local = threading.local()


def func_factorial(*num_lst):
    return tuple([x for x in range(1, num + 1) if num % x == 0] for num in num_lst)


def run_by_tpe(num_lst, max_workers=6):
    print('\nUse ThreadPoolExecutor: \n')
    for process in range(1, max_workers + 1):
        threaded_start = time.time()
        with concurrent.futures.ThreadPoolExecutor(process) as executor:
            result = executor.map(func_factorial, num_lst)
            for num, res in zip(num_lst, result):
                print(num, res)
        duration = time.time() - threaded_start
        print(f'Thread {range(1, max_workers + 1).index(process) + 1} time: {duration}')
        print(f'\n{"-" * 80}\n')


def run_by_ppe(num_lst, max_process=7):
    print('Use ProcessPoolExecutor: ')
    for process in range(1, max_process + 1):
        start_multi = time.time()
        with concurrent.futures.ProcessPoolExecutor(process) as executor:
            result = zip(num_lst, executor.map(func_factorial, num_lst))
            for num, res in zip(num_lst, result):
                print(num, res)
        duration = time.time() - start_multi
        print(f'for {process} process time is {duration}\n')
    print(f'{"-" * 80}')


def main():
    num_lst = [random.randint(5000000, 100000000) for _ in range(3)]
    run_by_tpe(num_lst)
    run_by_ppe(num_lst)


if __name__ == '__main__':
    main()
