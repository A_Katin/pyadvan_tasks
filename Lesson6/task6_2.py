# Створіть три функції, одна з яких читає файл на диску із заданим ім'ям та перевіряє наявність рядка «Wow!».
# Якщо файлу немає, то засипає на 5 секунд, а потім знову продовжує пошук по файлу. Якщо файл є, то відкриває його
# і шукає рядок «Wow!». За наявності цього рядка закриває файл і генерує подію, а інша функція чекає на цю подію
# і у разі її виникнення виконує видалення цього файлу. Якщо рядки «Wow!» не було знайдено у файлі,
# то засипати на 5 секунд. Створіть файл руками та перевірте виконання програми.
import os
from time import sleep
from threading import Thread

FILE_NAME = 'search_Wow.txt'
SEARCH_SYMBOLS = 'Wow!'


def del_file():
    if os.path.isfile(FILE_NAME) and os.access(FILE_NAME, os.R_OK):
        os.remove(FILE_NAME)
        print("File Deleted successfully")
        call_thread(Thread(target=main))
    else:
        print("File does not exist")
        sleep(5)


def call_thread(thread):
    # thread.daemon = True
    thread.start()


def main():
    flg_loop = True
    while flg_loop:
        try:
            text_file = open(FILE_NAME, encoding='utf-8')
        except IOError as e:
            print('could not open file')
            sleep(5)
        else:
            with text_file:
                for row in text_file.readlines():
                    if SEARCH_SYMBOLS in row:
                        flg_loop = False
                        break
                else:
                    print('no searching symbols in file')
                    sleep(5)
    call_thread(Thread(target=del_file))


if __name__ == '__main__':
    main()
