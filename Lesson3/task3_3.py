# Попрацюйте зі створенням власних діалектів, довільно вибираючи правила для CSV-файлів.
# Зареєструйте створені діалекти та попрацюйте, використовуючи їх зі створенням/читанням файлом.
import csv
from faker import Faker

FILE_NAME = 'data_file_task3_3.csv'
csv.register_dialect('my_dialect', delimiter='|', skipinitialspace=True, quoting=csv.QUOTE_ALL)


def make_test_file():
    fake = Faker()
    with open(FILE_NAME, 'w', newline='', encoding="utf-8") as csvfile:
        fieldnames = ['name', 'phone', 'e-mail', 'address', 'id_card']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, dialect='my_dialect')
        writer.writeheader()
        # row_text = ''
        # for _ in range(100):
        #     row_text += fake.dsv(dialect='my_dialect',
        #                          header=('name', 'phone', 'e-mail', 'address', 'id_card'),
        #                          data_columns=('{{name}}', '{{phone_number}}','{{email}}', '{{address}}', '{{ssn}}'))
        # writer.writerow(row_text)
        for _ in range(100):
            writer.writerow({'name': fake.name(), 'phone': fake.phone_number(), 'e-mail': fake.email(),
                             'address': fake.address(), 'id_card': fake.ssn()})


def main():
    make_test_file()
    with open(FILE_NAME, 'r') as myFile:
        dialect = csv.Sniffer().sniff(myFile.read(1024))
        myFile.seek(0)
        lines = csv.reader(myFile, dialect)
        for row in lines:
            print(row)


if __name__ == '__main__':
    main()
