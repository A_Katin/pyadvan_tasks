# Створіть таблицю «матеріали» з таких полів: ідентифікатор, вага, висота та додаткові характеристики матеріалу.
# Поле «додаткові  характеристики матеріалу» має зберігати у собі масив, кожен елемент якого є кортежем із двох значень:
# перше – назва характеристики, а друге – її значення.
from faker import Faker
import json

FILE_NAME = 'data_file_task3_7_materials.json'


def make_table_materials():
    fake = Faker()
    materials_lst = []
    for _ in range(100):
        materials_lst.append({'id': fake.pystr_format(), 'weight': fake.pyint(1, 100), 'height': fake.pyint(1, 10),
                              'add_characteristics': [('barcode', fake.ean13()), ('color', fake.color_name()),
                                                      ('vendor_code', fake.ssn())]})
    with open(FILE_NAME, 'w') as jsonfile:
        json.dump(materials_lst, jsonfile)


def main():
    make_table_materials()


if __name__ == '__main__':
    main()
