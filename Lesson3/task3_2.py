# Створіть XML-файл із вкладеними елементами та скористайтеся мовою пошуку XPATH.
# Спробуйте здійснити пошук вмісту за створеним документом XML, ускладнюючи свої запити та додаючи нові елементи,
# якщо буде потрібно.
from datetime import datetime
from xml.etree import ElementTree as ET
from faker import Faker

FILE_NAME = 'data_file_task3_2.xml'


def error(message):
    print("Error: {} Try again.".format(message))


def get_str(prompt):
    while True:
        s = input(prompt).strip()
        if s:
            return s
        else:
            error("Input required.")


def main():
    fake = Faker('uk_UA')
    main_v = ET.Element('DECLAR')
    profiles = ET.SubElement(main_v, 'profiles')

    for _ in range(100):
        data = fake.simple_profile()
        profile = ET.SubElement(profiles, 'profile')
        for element, value in data.items():
            new_record = ET.SubElement(profile, element)
            if element == 'birthdate':
                new_record.text = f'{value.day:02}-{value.month:02}-{value.year}'
            else:
                new_record.text = value

    tree = ET.ElementTree(main_v)
    tree.write(FILE_NAME, encoding='utf-8')

    while True:
        print('\n{:_^50}'.format('Поиск по XML файлу с помощью XPath запросов'))
        print('*'*50)
        print('\n{:>25}'.format('1. Посчитать количество мужчин в списке'))
        print('{:>25}'.format('2. Вывести именинниц этого месяца'))
        print('{:>25}'.format('3. Найти логины, начинающиеся на указанную букву'))
        print('\n{:>25}'.format('\t[Q] для выхода'))
        get_act = get_str('\nВведите дальнейшее действие: ')
        tree = ET.parse(FILE_NAME)
        match get_act:
            case ('Q' | 'q'):
                break
            case "1":
                print('\nКоличество мужчин в списке: ', len(tree.findall(".//profile[sex='M']")))
            case "2":
                hbd_lst = []
                for el in tree.findall(".//profile[sex='F']/name"):
                    bd = tree.find(f".//profile[name='{el.text}']/birthdate").text
                    if bd[3:5] == str(datetime.now().month):
                        hbd_lst.append((el.text, bd))
                if len(hbd_lst) > 0:
                    print('\nИменинницы этого месяца:')
                    for a in hbd_lst:
                        print(f'{a[0]:<22}: {a[1]:<20}')
                else:
                    print('No data')
            case "3":
                users_lst = []
                start_symbol = get_str('Введите начальную букву: ')
                for nick in tree.findall(".//profile/username"):
                    if nick.text[0] == start_symbol.lower():
                        users_lst.append(nick.text)
                if len(users_lst) > 0:
                    print('\nНайдены следующие логины:')
                    for u in users_lst:
                        print(f'{u}')
                else:
                    print('No data')
            case _:
                break


if __name__ == '__main__':
    main()
