# Для таблиці «матеріалу» з додаткового завдання створіть функцію користувача,
# яка приймає необмежену кількість полів і повертає їх конкатенацію.
from faker import Faker
import json

FILE_NAME = 'data_file_task3_5_materials.json'


def adder(*args):
    big_str = ''
    for a in args:
        big_str += str(a)
    return big_str


def make_table_materials():
    fake = Faker()
    materials_lst = []
    for _ in range(100):
        materials_lst.append({'id': fake.pystr_format(), 'weight': fake.pyint(1, 100), 'height': fake.pyint(1, 10),
                              'add_characteristics': [('barcode', fake.ean13()), ('color', fake.color_name()),
                                                      ('vendor_code', fake.ssn())]})
    with open(FILE_NAME, 'w') as jsonfile:
        json.dump(materials_lst, jsonfile)


def main():
    make_table_materials()
    with open(FILE_NAME, 'r') as json_data:
        data = json.load(json_data)
        for dict_string in data:
            print(adder(dict_string.get('id'), dict_string.get('weight'), dict_string.get('height'),
                  dict_string.get('add_characteristics')[0][1],
                  dict_string.get('add_characteristics')[1][1],
                  dict_string.get('add_characteristics')[2][1]))


if __name__ == '__main__':
    main()
