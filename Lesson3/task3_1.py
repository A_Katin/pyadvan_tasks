# Створіть прості словники та конвертуйте їх у JSON. Збережіть JSON у файлі та спробуйте завантажити дані з файлу.
import json
from faker import Faker


FILE_NAME = 'data_file_task3_1.json'


def main():
    fake = Faker('uk_UA')
    data_gen_dict = {'user' + str(i): (fake.name(), fake.phone_number(), fake.ssn()) for i in range(1, 11)}
    with open(FILE_NAME, "w") as out_file:
        json.dump(data_gen_dict, out_file)
    print('Data before loading to file')
    for k,v in data_gen_dict.items():
        print(f'{k}: {v}')
    print('='*40)
    print('\nData after reading from file')
    with open(FILE_NAME, "r") as in_file:
        data = json.load(in_file)
        if isinstance(data, list):
            for i in data:
                print(i)
        elif isinstance(data, dict):
            for key, value in data.items():
                print(f'{key}: {value}')


if __name__ == '__main__':
    main()
