# Створіть таблицю «матеріали» з таких полів: ідентифікатор, вага, висота та додаткові характеристики
# матеріалу. Поле «додаткові характеристики матеріалу» має зберігати у собі масив, кожен елемент
# якого є кортежем із двох значень: перше – назва характеристики, а друге – її значення.
# Cтворіть користувальницьку агрегатну функцію, яка рахує середнє значення ваги всіх матеріалів вислідної вибірки
# й округляє значення до цілого.
from faker import Faker
import json
import functools


FILE_NAME = 'data_file_task3_4_materials.json'


def make_table_materials():
    fake = Faker()
    materials_lst = []
    for _ in range(100):
        materials_lst.append({'id': fake.pystr_format(), 'weight': fake.pyint(1, 100), 'height': fake.pyint(1, 10),
                              'add_characteristics': [('barcode', fake.ean13()), ('color', fake.color_name()),
                                                      ('vendor_code', fake.ssn())]})
    with open(FILE_NAME, 'w') as jsonfile:
        json.dump(materials_lst, jsonfile)


def get_average_value():
    with open(FILE_NAME, 'r') as json_data:
        data = json.load(json_data)
        weight_lst = list()
        for dict_string in data:
            weight_lst.append(dict_string.get('weight'))
        if len(weight_lst) > 0:
            average = functools.reduce(lambda i, j: i + j, weight_lst) / len(weight_lst)
            print("Среднее значение равно: ", round(average))
        else:
            print('No data')


def main():
    make_table_materials()
    get_average_value()


if __name__ == '__main__':
    main()
