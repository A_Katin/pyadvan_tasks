# Створіть функцію, яка формує CSV-файл на основі даних, введених користувачем через консоль.
# Файл має містити такі стовпчики: імена, прізвища, дати народження та місто проживання.
# Реалізуйте можливості перезапису цього файлу, додавання нових рядків до наявного файлу, рядкового читання
# з файлу та конвертації всього вмісту у формати XML та JSON.
import os
import csv
import json
from faker import Faker
from xml.etree import ElementTree as ET

CSV_FILE_NAME = 'data_file_task3_6.csv'
XML_FILE_NAME = 'data_file_task3_6.xml'
JSON_FILE_NAME = 'data_file_task3_6.json'
FIELD_NAMES = ['first_name', 'last_name', 'birthdate', 'address']
csv.register_dialect('my_dialect', delimiter='|', skipinitialspace=True, quoting=csv.QUOTE_ALL)


def error(message):
    print("Error: {} Try again.".format(message))


def get_str(prompt):
    while True:
        s = input(prompt).strip()
        if s:
            return s
        else:
            error("Input required.")


def convert_to_xml_file():
    main_branch = ET.Element('MAIN')
    rows = ET.SubElement(main_branch, 'rows')
    with open(CSV_FILE_NAME, 'r') as myFile:
        dialect = csv.Sniffer().sniff(myFile.read(1024))
        myFile.seek(0)
        data_row = csv.reader(myFile, dialect)
        first_line = True
        for row in data_row:
            if first_line:  # skip first line
                first_line = False
                continue
            profile = ET.SubElement(rows, 'row')
            for field in FIELD_NAMES:
                new_record = ET.SubElement(profile, field)
                new_record.text = row[FIELD_NAMES.index(field)]

    tree = ET.ElementTree(main_branch)
    tree.write(XML_FILE_NAME, encoding='utf-8')
    if os.stat(XML_FILE_NAME).st_size != 0:
        print('Файл успешно конвертирован в XML-файл ', XML_FILE_NAME)


def convert_to_json_file():
    with open(CSV_FILE_NAME, 'r') as data_file:
        dialect = csv.Sniffer().sniff(data_file.read(1024))
        data_file.seek(0)
        data_row = csv.reader(data_file, dialect)
        first_line = True
        data_lst = []
        for row in data_row:
            if first_line:  # skip first line
                first_line = False
                continue
            row_data_dict = {}
            for field in FIELD_NAMES:
                row_data_dict[field] = row[FIELD_NAMES.index(field)]
            data_lst.append(row_data_dict)
        with open(JSON_FILE_NAME, 'w') as jsonfile:
            json.dump(data_lst, jsonfile)
        if os.stat(JSON_FILE_NAME).st_size != 0:
            print('Файл успешно конвертирован в JSON-файл ', JSON_FILE_NAME)


def make_test_file():
    fake = Faker()
    with open(CSV_FILE_NAME, 'w', newline='', encoding="utf-8") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=FIELD_NAMES, dialect='my_dialect')
        writer.writeheader()
        for _ in range(100):
            data = fake.simple_profile()
            name_lst = data['name'].split()
            f_name = s_name = ''
            if len(name_lst) == 2:
                f_name = name_lst[0]
                s_name = name_lst[1]
            elif len(name_lst) == 3:
                f_name = name_lst[1]
                s_name = name_lst[2]
            bd_value = data['birthdate']
            bd_text = f'{bd_value.day:02}-{bd_value.month:02}-{bd_value.year}'
            writer.writerow({'first_name': f_name,
                             'last_name': s_name,
                             'birthdate': bd_text,
                             'address': data['address']})


def main():
    make_test_file()
    while True:
        print('\n{:_^60}'.format('Работа с файлом формата CSV'))
        print('*' * 60)
        print('\n{:>20}'.format('1. Добавить новую запись в файл'), '\t\t{:<20}'.format('2. Прочитать файл построчно'))
        print('{:>20}'.format('3. Конвертировать файл в XML-формат'),
              '\t{:>20}'.format('4. Конвертировать файл в JSON-формат'))
        print('\n{:>20}'.format('\t[Q] для выхода'))
        get_act = get_str('\nВведите дальнейшее действие: ')
        match get_act:
            case ('Q' | 'q'):
                break
            case "1":
                while True:
                    print('\n{:>20}'.format('1. Добавить новую запись в существующий файл'),
                          '\t{:<20}'.format('2. Добавить новую запись в новый файл'))
                    print('\n{:>20}'.format('\t[3] Назад'))
                    get_act_1 = get_str('\nВведите дальнейшее действие: ')
                    match get_act_1:
                        case '3':
                            break
                        case ("1" | "2"):
                            flag_rw = 'w'
                            if get_act_1 == "1":
                                flag_rw = 'a'
                            with open(CSV_FILE_NAME, flag_rw, newline='', encoding="utf-8") as csvfile:
                                writer = csv.DictWriter(csvfile, fieldnames=FIELD_NAMES, dialect='my_dialect')
                                if get_act_1 == "2":
                                    writer.writeheader()
                                writer.writerow({'first_name': get_str('\nВведите имя: '),
                                                 'last_name': get_str('Введите фамилию: '),
                                                 'birthdate': get_str('Введите дату рождения: '),
                                                 'address': get_str('Введите адрес: ')})
                                print('данные успешно внесены!')
            case "2":
                with open(CSV_FILE_NAME, 'r') as myFile:
                    dialect = csv.Sniffer().sniff(myFile.read(1024))
                    myFile.seek(0)
                    lines = csv.reader(myFile, dialect)
                    print()
                    for line in lines:
                        print(line)
            case "3":
                convert_to_xml_file()
            case "4":
                convert_to_json_file()
            case _:
                break


if __name__ == '__main__':
    main()
