import socket
from threading import Thread


def receive():
    while True:
        msg = client_sock.recv(BUFSIZ).decode("utf8")
        if msg == 'q':
            client_sock.close()
            break
        if not msg:
            break
        print(msg)


def send():
    while True:
        msg = input()
        client_sock.send(bytes(msg, "utf8"))
        if msg == 'q':
            break


HOST = 'localhost'
PORT = 50555

BUFSIZ = 1024

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_sock:
    client_sock.connect((HOST, PORT))
    receive_thread = Thread(target=receive)
    send_thread = Thread(target=send)
    receive_thread.start()
    send_thread.start()
    receive_thread.join()
    send_thread.join()


