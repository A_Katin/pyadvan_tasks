import socket
from threading import Thread


def accept_incoming_connections():
    """Sets up handling for incoming clients."""
    while True:
        client, client_address = server_sock.accept()
        print(f'{client_address[0]}:{client_address[1]} has connected.')
        client.send(bytes("Type your name and press Enter: ", "utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,)).start()


def handle_client(client):  # Takes client socket as argument.
    """Handles a single client connection."""

    name = client.recv(BUFSIZ).decode("utf8")
    welcome = f'Welcome {name}! If you want to quit, input [q]uit to exit. '
    client.send(bytes(welcome, "utf8"))
    msg = f'[{name}] has joined the chat!'
    broadcast(client, bytes(msg, "utf8"))
    clients[client] = name

    while True:
        msg = client.recv(BUFSIZ)
        if msg != bytes("q", "utf8") and len(msg) > 0:
            broadcast(client, msg, '>> [' + name + ']: ')
        else:
            client.send(bytes("q", "utf8"))
            client.close()
            del clients[client]
            broadcast(client, bytes(f'[{name}] has left the chat.', 'utf8'))
            break


def broadcast(client, msg, prefix=""):  # prefix is for name identification.
    """Broadcasts a message to all the clients."""

    for sock in clients:
        if sock != client:
            sock.send(bytes(prefix, "utf8") + msg)


clients = {}
addresses = {}

HOST = ''
PORT = 50555
BUFSIZ = 1024

if __name__ == "__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_sock:
        server_sock.bind((HOST, PORT))
        server_sock.listen(5)
        print("Waiting for connection...")
        ACCEPT_THREAD = Thread(target=accept_incoming_connections())
        ACCEPT_THREAD.start()
        ACCEPT_THREAD.join()
