# Створіть HTTP-клієнта, який прийматиме URL ресурсу, тип методу та словник як передавальні дані (опціональний).
# Виконувати запит з отриманим методом на отриманий ресурс, передаючи дані відповідним методом,
# та друкувати на консоль статус-код, заголовки та тіло відповіді.
import tkinter as tk
import requests
import re


class MethodError(Exception):
    pass


dic_operations = {
    'get': [lambda url_: requests.get(url_)],
    'post': [lambda url_: requests.post(url_)],
    'put': [lambda url_: requests.put(url_)],
    'delete': [lambda url_: requests.delete(url_)],
}


def get_operation(operation: str = ''):
    try:
        return dic_operations[operation][0](entry_input_url.get())
    except KeyError:
        return 'Неизвестная операция \"%s\"' % operation


def check_url():
    str_url = entry_input_url.get()
    if len(str_url) > 0:
        if 'http' not in str_url:
            entry_input_url.insert(0, '')
            entry_input_url.insert(0, 'http://')
        return True
    return False


def pars_http_response(_response):
    headers_dict = {}
    t_response = str(_response.headers)
    t_response = re.sub('|'.join("'{}"), '', t_response)
    lines = t_response.split(',')
    for index, line in enumerate(lines):
        line = line.strip()
        line = line.strip('\r')
        key, _, value = line.partition(':')
        headers_dict.setdefault(key.strip(), value.strip())
    content = _response.text
    content = re.sub('|'.join("\n"), '', content)
    return _response.status_code, headers_dict, content


def command_get(http_method):
    if check_url():
        if http_method in ['delete', 'put']:
            text = '''спецификация HTML не позволяет создавать формы, 
            отправляющие данные иначе, чем через GET или POST'''
            text_att = tk.Label(root, text=text, font=('Helvetica', 10, 'bold'))
            text_att.place(y=190)
            if http_method == 'put':
                raise MethodError('использование метода PUT вызывает ошибку') from None
        try:
            status_code, headers, content = pars_http_response(get_operation(http_method))
            text_status_code = tk.Label(root, text=f' Статус-код: {status_code} ', fg='red')
            text_status_code.place(y=35)
            text_headers = tk.Label(root, text=f' Headers: {headers} ', fg='blue')
            text_headers.place(y=55)
            text_content = tk.Label(root, text=f' Body: {content} ', fg='blue')
            text_content.place(x=0, y=85)
        except MethodError as err_msg:
            text_att = tk.Label(root, text=err_msg, font=('Helvetica', 10, 'bold'), fg='red')
            text_att.place(y=190)


root = tk.Tk()
root.title("HTTP-CLIENT")
root.geometry('450x300')

entry_input_url = tk.Entry(root)
entry_input_url = tk.Entry(root, width=150)
entry_input_url.pack(anchor='nw', padx=10, pady=15)
entry_input_url.insert(0, "example.com")

but1 = tk.Button(root, width='15', text='GET', command=lambda: command_get('get'))
but1.place(x=20, y=233)
but2 = tk.Button(root, width='15', text='POST', command=lambda: command_get('post'))
but2.place(x=160, y=233)
but1 = tk.Button(root, width='15', text='PUT', command=lambda: command_get('put'))
but1.place(x=300, y=233)
but2 = tk.Button(root, width='15', text='DELETE', command=lambda: command_get('delete'))
but2.place(x=160, y=263)

root.mainloop()
