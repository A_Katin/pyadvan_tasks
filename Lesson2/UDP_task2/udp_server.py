import socket
import time
from Lesson2.UDP_task2 import udp_client


def run_udp_server():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        print('\n{:^50}'.format('UDP-server is online. Waiting for connection'))
        print('{:*^50}'.format('*'))
        sock.bind(('localhost', 50550))
        while True:
            udp_client.run_udp_client()
            data = sock.recv(1024)
            if not data:
                print('\n{:20}'.format('UDP-server is offline.'))
                break
            print(f'>> network device {data.decode("utf-8")} connected successfully')
            time.sleep(1)
