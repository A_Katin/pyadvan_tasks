import socket
import random
from string import ascii_lowercase


def get_rand_mac_adr():
    ''' типа генератор МАС-адресов '''
    return '{:02}:{}:{}:{}:{}:{:02}'.format(random.randint(0, 10),
                                      random.choice([*ascii_lowercase]) + str(random.randint(0, 10)),
                                      str(random.randint(0, 10)) + random.choice([*ascii_lowercase]),
                                      random.choice([*ascii_lowercase]) + random.choice([*ascii_lowercase]),
                                      str(random.randint(0, 10)) + random.choice([*ascii_lowercase]),
                                      random.randint(0, 10))


def run_udp_client():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        for _ in range(5):
            sock.sendto(bytes(get_rand_mac_adr(), 'utf-8'), ('localhost', 50550))
        sock.sendto(bytes("", 'utf-8'), ('localhost', 50550))


if __name__ == '__main__':
    run_udp_client()
