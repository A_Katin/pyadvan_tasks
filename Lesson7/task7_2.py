# Створіть два класи Directory (тека) і File (файл) з типами (анотацією).
# Клас Directory має мати такі поля:
# ·        назва (name типу str);
# ·        батьківська тека (root типу Directory);
# ·        список файлів (список типу files, який складається з екземплярів File);
# ·        список підтек (список типу sub_directories, який складається з екземплярів Directory).
#
# Клас Directory має мати такі поля:
# ·        додавання теки до списку підтек (add_sub_directory, який приймає екземпляр Directory та
#           присвоює поле root для приймального екземпляра);
# ·        видалення теки зі списку підтек (remove_sub_directory, який приймає екземпляр Directory та обнуляє поле root.
#           Метод також видаляє теку зі списку sub_directories);
# ·        додавання файлу в теку (add_file, який приймає екземпляр File і присвоює йому поле directory –
#           див. клас File нижче);
# ·        видалення файлу з теки (remove_file, який приймає екземпляр File та обнуляє у нього поле directory.
#           Метод видаляє файл зі списку files).
#
# Клас File має мати такі поля:
# ·        назва (name типу str);
# ·        тека (Directory типу Directory).
from typing import List, Any, Dict
from pathlib import Path
import os


class File:
    name: str
    directory: 'Directory'


class Directory:
    name: str
    root: 'Directory'
    files: List['File'] = []
    sub_directories: List['Directory'] = []

    @staticmethod
    def get_from_list(cls_name: str, lst_num: int = 0) -> Any:
        dict_cls_lst: Dict[int, List[Any]] = {0: Directory.sub_directories, 1: Directory.files}
        if dict_cls_lst[lst_num] is not None:
            for cls_obj in dict_cls_lst[lst_num]:
                if cls_obj.name == cls_name:
                    return cls_obj
        return None

    def add_sub_directory(self, sub_directory: 'Directory') -> None:
        ''' додавання теки до списку підтек '''
        if isinstance(sub_directory, Directory):
            sub_directory.root = self
            self.sub_directories.append(sub_directory)

    def remove_sub_directory(self, sub_directory: 'Directory') -> None:
        ''' видалення теки зі списку підтек '''
        if isinstance(sub_directory, Directory):
            del sub_directory.root
            self.sub_directories.remove(sub_directory)

    def add_file(self, file: 'File') -> None:
        ''' додавання файлу в теку '''
        if isinstance(file, File):
            self.files.append(file)
            file.directory = self

    def remove_file(self, file: 'File'):
        '''видалення файлу з теки'''
        if isinstance(file, File):
            del file.directory
            self.files.remove(file)


def main() -> None:
    work_path = Path(Path.home())
    main_dir = Directory()
    main_dir.name = 'Project'
    folders = [el for el in os.listdir(work_path) if os.path.isdir(str(work_path) + '\\' + el)]
    for address1 in folders:
        dir_obj = Directory()
        dir_obj.name = address1
        main_dir.add_sub_directory(dir_obj)
    for subdir in main_dir.sub_directories:
        print(subdir.name, subdir.root.name)

    files = [el for el in os.listdir(work_path) if os.path.isfile(str(work_path) + '\\' + el)]
    for address2 in files:
        file_obj = File()
        file_obj.name = address2
        main_dir.add_file(file_obj)
    for file in main_dir.files:
        print(file.name, file.directory.name)

    print()
    for address3 in folders:
        del_sub_dir = Directory.get_from_list(address3)
        main_dir.remove_sub_directory(del_sub_dir)
    print(main_dir.sub_directories, len(main_dir.sub_directories))

    print()
    for address4 in files:
        del_file = Directory.get_from_list(address4, 1)
        main_dir.remove_file(del_file)
    print(main_dir.files, len(main_dir.files))


if __name__ == '__main__':
    main()
