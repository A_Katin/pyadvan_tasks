# Використовуючи модуль sqlite3 та модуль smtplib, реалізуйте реальне додавання користувачів до бази.
# Мають бути реалізовані такі функції та класи:
# ·        клас користувача, що містить у собі такі методи:
# get_full_name (ПІБ з поділом через пробіл: «Петров Ігор Сергійович»),
# get_short_name (формату ПІБ: «Петров І. С.»),
# get_age (повертає вік користувача, використовуючи поле birthday типу datetime.date);
# метод __str__ (повертає ПІБ та дату народження);
# ·        функція реєстрації нового користувача (приймаємо екземпляр нового користувача та
# відправляємо email на пошту користувача з листом подяки).
# ·        функція відправлення email з листом подяки.
# ·        функція пошуку користувачів у таблиці users за іменем, прізвищем і поштою.
#
# Протестувати цей функціонал, використовуючи заглушки у місцях надсилання пошти.
# Під час штатного запуску програми вона має відправляти повідомлення на вашу реальну поштову скриньку
# (необхідно налаштувати SMTP, використовуючи доступи від провайдера вашого email-сервісу).
import sys
import users


def error(message):
    print("Error: {} Try again.".format(message))


def get_str(prompt, fl_not_empty: bool = True):
    while True:
        s = input(prompt).strip()
        if fl_not_empty:
            if s:
                return s
            else:
                error("Input required.")
        else:
            return s


def find_user_on_db(**kwargs):
    users_spr = users.Users().users
    users_row = ''
    if len(kwargs.keys()) == 1:
        for find, value in kwargs.items():
            for user in users_spr:
                match find:
                    case 'first_name':
                        if user.first_name.lower() == value.lower():
                            users_row += f'{user.first_name}\t{user.surname}\t{user.email}\n'
                    case 'surname':
                        if user.surname.lower() == value.lower():
                            users_row += f'{user.surname}\t{user.first_name}\t{user.email}\n'
                    case 'email':
                        if user.email.lower() == value.lower():
                            users_row += f'{user.email}\t{user.first_name}\t{user.surname}\n'
                    case 'username':
                        if user.username.lower() == value.lower():
                            users_row += f'{user.username}\t{user.first_name}\t{user.surname}\t{user.email}\n'
    elif len(kwargs.keys()) >= 1:
        value = kwargs.get('email')
        for user in users_spr:
            if user.surname == value:
                users_row += f'{user.first_name} {user.surname:>15}\t{user.email:^15}\n'
            elif user.email == value:
                users_row += f'{user.first_name} {user.surname:>15}\t{user.email:^15}\n'
            elif user.first_name == value:
                users_row += f'{user.first_name} {user.surname:>15}\t{user.email:^15}\n'
    return users_row


def add_user():
    while True:
        print('\nAddition user to the database')
        print('*' * 30)
        username = get_str('\nEnter the username: ')
        if username.lower() == 'quit':
            break
        if find_user_on_db(username=username) == '':
            email = get_str('\nEnter the email: ')
            if email.lower() == 'quit':
                break
            if find_user_on_db(email=email) == '':
                surname = get_str('\nEnter the surname: ')
                first_name = get_str('\nEnter the first name: ')
                middle_name = get_str('\nEnter the middle name: ', False)
                birthday_str = input('\nEnter the birthday : ').lower().strip()
                birthday = users.get_timestamp_from_str(birthday_str)
                new_user = users.User(username=username,
                                      birthday=birthday,
                                      surname=surname,
                                      first_name=first_name,
                                      middle_name=middle_name,
                                      email=email)
                users.add_user_to_db(new_user)
                break
            else:
                print(f'The user with email "{email}" already exists, chose another or enter Quit for exit')
        else:
            print(f'The username "{username}" already exists, chose another or enter Quit for exit')


def console_talk():
    while True:
        print('\nThe program of real addition users to the database')
        print('*' * 50)
        print(f'\n1. All users{"2. Find user":>18}{"3. Add user":>18}')
        print(f'\n\t{"[Q] exit":>15}')
        get_act = get_str('\nChoose the next action: ')
        match get_act:
            case ('Q' | 'q' | 'й' | 'Й'):
                break
            case '1':
                users_spr = users.Users()
                print()
                print(users_spr)
            case '2':
                userinfo = get_str('\nFor searching enter the name, surname or email: ')
                find_user = find_user_on_db(surname=userinfo, first_name=userinfo, email=userinfo)
                if find_user == '':
                    print('\nNo data')
                else:
                    print()
                    print(find_user)

            case '3':
                add_user()


def main():
    console_talk()
    return 0


if __name__ == '__main__':
    sys.exit(main())
