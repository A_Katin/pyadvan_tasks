CREATE TABLE IF NOT EXISTS users(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username text,
    surname text,
    first_name text,
    middle_name text,
    birthday INTEGER,
    email text
);