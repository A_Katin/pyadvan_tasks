import smtplib
from typing import Dict, List, NamedTuple, Any
from email.mime.text import MIMEText
from datetime import datetime, timezone, date
import db
import re


def get_timestamp(y: int, m: int, d: int):
    if y >= 2015:
        y -= 5
    elif y <= 1970:
        y = 1971
    # return datetime.timestamp(datetime(y, m, d, 12, 0, 0, tzinfo=timezone.utc))
    return datetime.timestamp(datetime(y, m, d, 12))


def get_data(tmstmp):
    return datetime.fromtimestamp(tmstmp).date()


def get_timestamp_from_str(data_str):
    hbd_date = 0
    if data_str != '':
        date_lst = list(map(int, re.split('[/\-\.]', data_str)))
        if len(date_lst) > 0:
            try:
                hbd_date = get_timestamp(date_lst[2], date_lst[1], date_lst[0])
            except ValueError as error_msg:
                print("Error: {} Try again.".format(error_msg))
    return hbd_date


def add_user_to_db(user: 'User'):
    db.insert_to_db("users", {
        "username": user.username,
        "birthday": user.birthday,
        "surname": user.surname,
        "first_name": user.first_name,
        "middle_name": user.middle_name,
        "email": user.email
    })
    print('\nUser was successfully registered!')
    print(Users.send_email(user))


class User(NamedTuple):
    username: str
    birthday: int
    surname: str
    first_name: str
    middle_name: str
    email: str


class Users:
    users: List[User] = []

    def __init__(self):
        self.users = self._load_users(db.CURSOR)

    def _load_users(self, cursor) -> List[User]:
        users = Users._fetchall(
            "users", "username birthday surname first_name middle_name email".split(), {}, cursor
        )
        users = self._fill_data(users)
        return users

    @staticmethod
    def _fill_data(users: List[Dict]) -> List[User]:
        users_result = []
        for index, user in enumerate(users):
            users_result.append(User(
                username=user['username'],
                birthday=user['birthday'],
                surname=user['surname'],
                first_name=user['first_name'],
                middle_name=user['middle_name'],
                email=user['email']
            ))
        return users_result

    @staticmethod
    def _fetchall(table: str, columns: List[str], add_if_dict: dict, cursor) -> List[Dict[str, Any]]:
        columns_joined = ', '.join(columns)
        main_query_str = f'SELECT {columns_joined} FROM {table}'
        for i in range(len(add_if_dict)):
            fields_lst = list(add_if_dict.keys())
            field = fields_lst[i]
            add_if_str = ''
            if len(add_if_dict[field]) > 1:
                for y in range(len(add_if_dict[field])):
                    add_if_str += f'{field} {add_if_dict[field][y][0]} {str(add_if_dict[field][y][1])}'
                    if y != len(add_if_dict[field]) - 1:
                        add_if_str += f' AND '
            else:
                add_if_str += f'{field} {add_if_dict[field][0][0]} {str(add_if_dict[field][0][1])}'
                if i != len(add_if_dict) - 1:
                    add_if_str += f' AND '
            main_query_str += f' WHERE {add_if_str}'
        cursor.execute(main_query_str)
        rows = cursor.fetchall()
        result = []
        for row in rows:
            dict_row = {}
            for index, column in enumerate(columns):
                dict_row[column] = row[index]
            result.append(dict_row)
        return result

    @staticmethod
    def send_email(user: User):
        sender = 'aekaterinin@gmail.com'
        password = db.SMTP_PSW

        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.starttls()

        try:
            server.login(sender, password)
            msg = MIMEText(f'Thank you {user.first_name.capitalize()} {user.surname.capitalize()} for registration!')
            msg["Subject"] = "Thank you very much for registering!"
            server.sendmail(sender, sender, msg.as_string())

            return "The message was sent successfully!"
        except Exception as _ex:
            return f"{_ex}\nCheck your login or password please!"

    @staticmethod
    def get_full_name(user: User):
        return f'{user.surname.capitalize()} {user.first_name.capitalize()} {user.middle_name.capitalize()}'

    @staticmethod
    def get_short_name(user: User):
        return f'{user.surname.capitalize()} {user.first_name[0].upper()}. {user.middle_name[0].upper()}.'

    @staticmethod
    def get_age(user: User):
        return f'{datetime.now(tz=timezone.utc).year - get_data(user.birthday).year}'

    def __str__(self):
        usrs_row = ''
        for user in self.users:
            usrs_row += f'{self.get_full_name(user):35}\t{get_data(user.birthday).strftime("%d/%m/%Y"):^15}\n'
        return usrs_row
