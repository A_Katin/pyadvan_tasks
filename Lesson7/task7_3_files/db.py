from faker import Faker
from datetime import datetime
import random
import sqlite3 as sq
import os


NAME_DB = 'task7_3_files/users.db'
CONN = sq.connect(NAME_DB)
CURSOR = CONN.cursor()
SMTP_PSW = os.getenv("SMTP_PSW")


def get_name(symbol_search: str) -> str:
    dict_names = {
        'пан': ['Валерій', 'Євгеній', 'Йосип', 'Олег', 'Віталій', 'Георгій', 'Лев', 'Руслан', 'Іван', 'Леонід', 'Ілля'],
        'пані': ['Людмила', 'Євгенія', 'Маргарита', 'Марина', 'Діана', 'Аліна', 'Любов', 'Тетяна', 'Поліна', 'Катерина',
                 'Яна', 'Ганна'],
        'M': ['Миколайович', 'Володимирович', 'Олександрович', 'Іванович', 'Васильович', 'Сергійович',
              'Вікторович', 'Михайлович', 'Павлович', 'Ілліч', 'Григорович'],
        'F': ['Миколаївна', 'Володимирівна', 'Олександрівна', 'Іванівна', 'Василівна', 'Сергіївна',
              'Вікторівна', 'Михайлівна', 'Богданівна', 'Андріївна', 'Кузьмівна']
    }
    value = dict_names.get(symbol_search, 0)
    if value == 0:
        return symbol_search
    else:
        return random.choice(value)


def insert_to_db(table: str, column_values: dict):
    columns = ', '.join(column_values.keys())
    values = [tuple(column_values.values())]
    placeholders = ", ".join("?" * len(column_values.keys()))
    CURSOR.executemany(
        f"INSERT INTO {table} "
        f"({columns}) "
        f"VALUES ({placeholders})",
        values)
    CONN.commit()


def _init_db() -> None:
    with sq.connect(NAME_DB) as con:
        cur = con.cursor()
        with open('task7_3_files/create_Users_db.sql', "r") as f:
            sql = f.read()
        cur.executescript(sql)

        start_users_lst = []

        fake = Faker('uk_UA')
        for i in range(100):
            data = fake.simple_profile()
            bth_day = data['birthdate']
            year = bth_day.year
            if year >= 2015:
                year -= 5
            elif year <= 1970:
                year = 1971
            start_users_lst.append(
                (data['username'],
                 data['name'].split()[1],
                 get_name(data['name'].split()[0]),
                 get_name(data['sex']),
                 datetime.timestamp(datetime(year, bth_day.month, bth_day.day, 12)),
                 data['mail']))

        query = "INSERT INTO users VALUES(NULL,?,?,?,?,?,?);"
        cur.executemany(query, start_users_lst)

        con.commit()


def check_db_exists():
    cursor = CONN.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='users'")
    table_exists = cursor.fetchall()
    if table_exists:
        return
    _init_db()


check_db_exists()
